from flask import Flask
from flask import render_template
from flask import login_manager
app = Flask(__name__)

login_manager = LoginManager()
login_manager.init_app(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():

        login_user(user)

        flask.flash('Taime ça grosse truche hein !? Taime te connecter.')

        next = flask.request.args.get('Suivant')

        if not is_safe_url(next):
            return flask.abort(400)

        return flask.redirect(next or flask.url_for('index'))
    return flask.render_template('login.html', form=form)